//
//  ContactViewController.swift
//  WeekendAssigment
//
//  Created by Femi Oladiji on 06/03/2020.
//  Copyright © 2020 Femi Oladiji. All rights reserved.
//

import UIKit


class ContactTableView: UITableViewCell{
    
    @IBOutlet weak var myphoneImage: UIImageView!
}







class ContactViewController: UIViewController {
    
    
    

    enum phoneContactError: Error {
        case emptyNameField
        case emptyNumberField
    }
    

    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var phoneNumber: UITextField!
    @IBOutlet weak var contactTableView: UITableView!
    
    var model: ContactModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        model = ContactModel()
        contactTableView.dataSource = self
        contactTableView.delegate = self
    }
    
    @IBAction func insertToContact(_ sender: UIButton) {
        
        if name.text?.isEmpty ?? true {
            let alert = UIAlertController(title: "Invalid Name", message: " Type in a name", preferredStyle: .alert)
            let action = UIAlertAction(title: "Cancle", style: .cancel, handler: nil)
            alert.addAction(action)
            
            self.present(alert, animated: true, completion: nil)
        } else{
            guard let contactName = name.text?.uppercased() , contactName.count > 0 else {return}
            let contactNumber = phoneNumber.text ?? " "
            let newContact = Contact(name: contactName, phoneNumber: contactNumber)
            model.addContactRow(newContact)
            name.text = ""
            phoneNumber.text = ""
            contactTableView.reloadData()
        }
    }
    
    
    func showActionSheet(_ indexPath: IndexPath) throws {
        let callMe = model.add(at: indexPath)
        guard let cName = callMe?.name,
            let cNumber = callMe?.phoneNumber,
            !cNumber.isEmpty
            else{
                throw phoneContactError.emptyNumberField
        }
        
        let alertController = UIAlertController(title: "Do you want to call them", message: "Hey \(cName) just meet you, and heres their number \(cNumber) so call them maybe?", preferredStyle: .actionSheet)
        
        let phone = UIAlertAction(title: "Via Phone", style: .default, handler: { (action) -> Void in
            print("Ok button tapped")
        })
        
        let skype = UIAlertAction(title: "Via Skype", style: .default, handler: { (action) -> Void in
            print("Ok button tapped")
        })
        
        let whatsApp = UIAlertAction(title: "Via What's App", style: .default, handler: { (action) -> Void in
            print("Ok button tapped")
        })
        
        let deleteButton = UIAlertAction(title: "How About No", style: .destructive, handler: { (action) -> Void in
            print("Delete button tapped")
        })
        
        
        alertController.addAction(phone)
        alertController.addAction(skype)
        alertController.addAction(whatsApp)
        alertController.addAction(deleteButton)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showNoNumberSheet() {
        let alert = UIAlertController(title: "No Number", message: " Cant Call", preferredStyle: .alert)
        let action = UIAlertAction(title: "Cancle", style: .cancel, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
}

extension ContactViewController : UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return model.numberOfSection
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        model.numberOfRow(in: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let numberCell = tableView.dequeueReusableCell(withIdentifier: "contactCell", for: indexPath)
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "contactCell", for: indexPath) as? ContactTableView else { return numberCell }
        
        
        guard  let userInformation = model.add(at: indexPath) else {
            return cell
        }
        
        cell.textLabel?.text = userInformation.name
        cell.detailTextLabel?.text = userInformation.phoneNumber
        cell.myphoneImage.isHidden = numberCell.detailTextLabel?.text?.isEmpty ?? true ? true : false

        
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return String(model.sections[section])        }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        do {
            try showActionSheet(indexPath)
        } catch (phoneContactError.emptyNumberField) {
            showNoNumberSheet()
        } catch {
            print("n/a")
        }
        
    }
    
}

extension ContactViewController : UITableViewDelegate{}


