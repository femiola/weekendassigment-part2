//
//  ContactModel.swift
//  WeekendAssigment
//
//  Created by Femi Oladiji on 06/03/2020.
//  Copyright © 2020 Femi Oladiji. All rights reserved.
//

import Foundation

struct Contact {
    let name : String
    let phoneNumber : String
}

class ContactModel {
    private var cInformation: [Character:[Contact]]
    init() {
        cInformation = [:]
    }
}

extension Contact : Equatable {
    static func == (phoneContact1: Contact, phonecontact2: Contact) -> Bool {
        return phoneContact1.name.lowercased() == phonecontact2.name.lowercased() &&
            phonecontact2.phoneNumber == phonecontact2.phoneNumber
    }
}

extension ContactModel{
    var sections:[Character]{
        return cInformation.keys.sorted()
    }
    
    var numberOfSection: Int{
        return sections.count
    }
    
    func firstLetterIndex(_ firstLetterName: Character) -> Bool {
        return cInformation.keys.contains(firstLetterName)
    }
    
    func numberOfRow(in section: Int) -> Int {
        
        guard section < sections.count else { return 0 }
        let key = sections[section]
        guard let values = cInformation[key] else { return 0 }
        return values.count
    }
    
    func addContactRow(_ contact: Contact) {
        guard let key = contact.name.first else {
            return
        }
        for ContactSection in cInformation.values {
            if ContactSection.contains(contact) {
                return
            }
        }
        if firstLetterIndex(key) {
            cInformation[key]?.append(contact)
            cInformation[key]?.sort(by: {$0.name > $1.name})
        } else {
            let newContactSection = [contact]
            cInformation[key] = newContactSection
        }
    }
    
    func add(at indexPath: IndexPath) -> Contact? {
        guard indexPath.section < sections.count else { return nil }
        let key = sections[indexPath.section]
        guard let getSection = cInformation[key], indexPath.row < getSection.count else { return nil }
        return getSection[indexPath.row]
    }
}

